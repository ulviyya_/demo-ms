package az.microservices.loans.service;

import az.microservices.common.dto.User;
import az.microservices.loans.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {


    private final UserRepository userRepository;

    public User createUser(User user) {
        return userRepository.save(user);
    }
}
