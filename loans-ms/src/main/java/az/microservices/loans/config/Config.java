package az.microservices.loans.config;


import az.microservices.common.config.KafkaConsumerConfig;
import az.microservices.common.config.ModelMapperConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Import({ModelMapperConfig.class,
        KafkaConsumerConfig.class})
@Configuration
public class Config {
}
