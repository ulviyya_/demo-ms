package az.microservices.loans.listeners;


import az.microservices.common.dto.User;
import az.microservices.loans.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.MessagingException;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Slf4j
@RequiredArgsConstructor
@Component
public class LoanUserListener {

    private final UserService userService;

    @KafkaListener(id = "2",
            topics = "user-events",
            groupId = "loans-user-group-id",
            containerFactory = "kafkaJsonListenerContainerFactory")
    public void userEventListener(User dto) throws MessagingException, IOException {
        log.info("Message received : {}", dto);
        userService.createUser(dto);
    }
}
