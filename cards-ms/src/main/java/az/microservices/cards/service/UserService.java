package az.microservices.cards.service;

        import az.microservices.cards.repository.UserRepository;
        import az.microservices.common.dto.User;
        import lombok.RequiredArgsConstructor;
        import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {


    private final UserRepository userRepository;

    public User createUser(User user) {
        return userRepository.save(user);
    }
}
